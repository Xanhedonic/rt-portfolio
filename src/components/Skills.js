import React from "react";
import skills from "../skills";
// import logos
import html5_logo from "../images/HTML5_logo.png";
import css3_logo from "../images/CSS3_logo.png";
import es6_logo from "../images/ES6_logo.png";
import mysql_logo from "../images/mySQL_logo.png";
import node_logo from "../images/Node_logo.png";
import react_logo from "../images/React_logo.png";

function Skills() {
  const logos = [
    html5_logo,
    css3_logo,
    es6_logo,
    mysql_logo,
    node_logo,
    react_logo,
  ];

  return (
    <section className='skills'>
      <h2 className='section-title'>Skills</h2>
      <div className='skills-display'>
        {skills.map((skill) => {
          const { id, name, info } = skill;
          return (
            <div className='skill-card' key={id}>
              <img src={logos[id - 1]} alt={name} />
              <h3 className='card-title'>{name}</h3>
              <p>{info}</p>
            </div>
          );
        })}
      </div>
    </section>
  );
}

export default Skills;
