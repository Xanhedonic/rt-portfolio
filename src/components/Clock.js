import React, { useState, useEffect } from "react";

function Clock() {
  const [time, setTime] = useState({
    hour: new Date().getHours(),
    minute: new Date().getMinutes(),
    second: new Date().getSeconds(),
  });

  useEffect(() => {
    const interval = setInterval(() => {
      setTime({
        hour: new Date().getHours(),
        minute: new Date().getMinutes(),
        second: new Date().getSeconds(),
      });
    }, 1000);

    // must clear Interval to not clog up RAM
    return () => {
      clearInterval(interval);
    };
  });

  let { hour, minute, second } = time;
  return (
    <div className='clock'>
      <h4>Time</h4>
      <div className='clock-display'>
        <p>{hour < 10 ? `0${hour}` : hour}</p>
        <p>:</p>
        <p>{minute < 10 ? `0${minute}` : minute}</p>
        <p>:</p>
        <p>{second < 10 ? `0${second}` : second}</p>
      </div>
    </div>
  );
}

export default Clock;
