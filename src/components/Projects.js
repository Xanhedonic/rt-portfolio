import React from "react";

function Projects() {
  return (
    <>
      <h2 className='section-title'>Projects</h2>
      <div className='projects dark-glass'>
        <div className='project-card' id='project3'>
          <h3>The Cocktail DB</h3>
          <p></p>
          <div className='project-btn'>
            <a
              href='https://rt-cocktaildb.netlify.app/'
              target='_blank'
              rel='noreferrer'
            >
              Website
            </a>
            <a
              href='https://gitlab.com/Xanhedonic/cocktaildb-react'
              target='_blank'
              rel='noreferrer'
            >
              Code
            </a>
          </div>
        </div>
        <div className='project-card' id='project4'>
          <h3>Menu Filter</h3>
          <p></p>
          <div className='project-btn'>
            <a
              href='https://rt-menufilter.netlify.app/'
              target='_blank'
              rel='noreferrer'
            >
              Website
            </a>
            <a
              href='https://gitlab.com/Xanhedonic/menu-filter'
              target='_blank'
              rel='noreferrer'
            >
              Code
            </a>
          </div>
        </div>
      </div>
      <h2 className='section-title'>Other mini projects</h2>
      <div className='projects dark-glass'>
        <div className='project-card' id='project1'>
          <h3>Javascript Keyboard Press Test</h3>
          <p></p>
          <div className='project-btn'>
            <a
              href='https://rt-keypress.netlify.app/'
              target='_blank'
              rel='noreferrer'
            >
              Website
            </a>
            <a
              href='https://gitlab.com/Xanhedonic/keypress'
              target='_blank'
              rel='noreferrer'
            >
              Code
            </a>
          </div>
        </div>
        <div className='project-card' id='project2'>
          <h3>Animation with CSS and JavaScript</h3>
          <p></p>
          <div className='project-btn'>
            <a
              href='https://rt-breath-animation.netlify.app/'
              target='_blank'
              rel='noreferrer'
            >
              Website
            </a>
            <a
              href='https://gitlab.com/Xanhedonic/relaxanimationcss'
              target='_blank'
              rel='noreferrer'
            >
              Code
            </a>
          </div>
        </div>
      </div>
    </>
  );
}

export default Projects;
