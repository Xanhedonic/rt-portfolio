import React from "react";
import { useGlobalContext } from "../context";

function Modal() {
  const { isModalOpen, closeModal } = useGlobalContext();

  if (isModalOpen) {
    setTimeout(closeModal, 1000);
  }

  return (
    <div
      className={`${
        isModalOpen ? "modal-overlay show-modal" : "modal-overlay"
      }`}
    >
      <div className='modal-container'>
        <h4>Copied</h4>
        {/* <button onClick={closeModal}>Close</button> */}
      </div>
    </div>
  );
}

export default Modal;
