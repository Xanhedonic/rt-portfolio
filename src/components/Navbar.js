import React from "react";
import { Link } from "react-router-dom";
import RTlogo from "../RTlogo.png";

function Navbar() {
  return (
    <nav className='navbar'>
      <Link to='/'>
        <img src={RTlogo} alt='Robertas Tupikas' className='logo' />
      </Link>
      <ul className='nav-links'>
        <li>
          <Link to='/'>Home</Link>
        </li>
        <li>
          <Link to='/about'>About</Link>
        </li>
        <li>
          <Link to='/contact'>Contact</Link>
        </li>
      </ul>
    </nav>
  );
}

export default Navbar;
