import React from "react";
import Skills from "../components/Skills";

function About() {
  return (
    <>
      <section className='section'>
        <h1 className='section-title'>About me</h1>
        <div className='section-inner dark-glass'>
          <p>
            I'm Robert, a beginner-junior level web developer currently
            mastering ReactJS Framework to create dynamic, responsive websites
            and user interfaces.
          </p>
        </div>
      </section>
      <Skills />
    </>
  );
}

export default About;
