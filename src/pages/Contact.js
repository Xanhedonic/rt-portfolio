import React from "react";
import { AiFillMail, AiFillLinkedin, AiFillGitlab } from "react-icons/ai";
import { Link } from "react-router-dom";
import Modal from "../components/Modal";
import { useGlobalContext } from "../context";

function Contact() {
  const { openModal } = useGlobalContext();

  const copyToClipboard = () => {
    navigator.clipboard.writeText("xonerix@gmail.com");
    openModal();
  };

  return (
    <section className='section'>
      <h1 className='section-title'>Contact me</h1>
      <Modal />
      <div className='section-inner social-section dark-glass'>
        <ul className='social-list'>
          <li>
            Click to copy E-mail to clipboard <br />
            <button onClick={copyToClipboard}>
              <AiFillMail className='social-icon' /> xonerix@gmail.com
            </button>
          </li>
          <hr />
          <li>
            <a
              href='https://linkedin.com/in/robertas-tupikas-4902ab84'
              target='_blank'
              rel='noreferrer'
            >
              <AiFillLinkedin className='social-icon' /> LinkedIn
            </a>
          </li>
          <li>
            <a
              href='https://gitlab.com/Xanhedonic/rt-portfolio'
              target='_blank'
              rel='noreferrer'
            >
              <AiFillGitlab className='social-icon' /> This project on GitLab
            </a>
          </li>
          <li>
            <Link to='/files/CV_Robertas_Tupikas.pdf' target='_blank' download>
              Download CV
            </Link>
          </li>
        </ul>
      </div>
    </section>
  );
}

export default Contact;
