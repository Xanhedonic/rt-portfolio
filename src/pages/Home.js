import React from "react";
import Clock from "../components/Clock";
import Projects from "../components/Projects";
import react_logo from "../images/React_logo.png";

function Home() {
  return (
    <>
      <section className='home-section'>
        <h1 className='home-title'>Web Development with ReactJS</h1>
        <img className='home-react-logo' src={react_logo} alt='React logo' />
        <Clock />
      </section>
      <Projects />
    </>
  );
}

export default Home;
