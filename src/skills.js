const skills = [
  {
    id: 1,
    name: "HTML5",
    image: "HTML5_logo.png",
    info: "Building blocks and the skeleton of any Website in the world",
  },
  {
    id: 2,
    name: "CSS3",
    image: "CSS3_logo.png",
    info:
      "Forming the look and feel of the webpage, making it responsive and easy on the eyes",
  },
  {
    id: 3,
    name: "Javascript ES6+",
    image: "ES6_logo.png",
    info:
      "No dynamic website is complete without the implementation of complex functions and features",
  },
  {
    id: 4,
    name: "MySQL",
    image: "mySQL_logo.png",
    info:
      "MySQL is the world's most popular open source database. With its proven performance, reliability and ease-of-use, MySQL has become the leading database choice for web-based applications",
  },
  {
    id: 5,
    name: "NodeJS + Express",
    image: "Node_logo.png",
    info:
      "As an asynchronous event-driven JavaScript runtime, Node.js is designed to build scalable network applications. Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.",
  },
  {
    id: 6,
    name: "ReactJS",
    image: "React_logo.png",
    info:
      "A Javascript library for building user interfaces and creating single page applications using independent components",
  },
];

export default skills;
